## Machine learning algorithms for games
Machine learning algorithms used in games
### Areas covered
- Character behaviours (using state machines)
- Locomotion (using steering behaviours)
- Map navigation (using graph search and reinforcement learning)
