## Path find for game ai
Path finding algorithms for game ai
### Algorithms included
- Breadth first search
- A star
- Q learning (reinforcement learning)
### Dependencies
- [Dgame](http://dgame-dev.de/)
- Steering
