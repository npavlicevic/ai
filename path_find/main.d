import std.stdio;
import map.map;

unittest {
  int rows = 16, cols = 16;
  Map *map = map_create(rows, cols);
  int[] row_visited;
  int[] col_visited;
  int[Size.grid_max * Size.grid_max] came_from;
  map_generate(map);
  auto goal = map_place_goal(map);
  writef("%s\n", goal);
  auto player = map_place_player(map);
  writef("%s\n", player);
  map_print(map);
  map_print_simple(map);
  map_bfs(map, came_from, goal[0], goal[1], player[0], player[1]);
  map_extract_path(row_visited, col_visited, came_from, map, goal[0], goal[1], player[0], player[1]);
  assert(row_visited[$ - 1] == goal[0]);
  assert(col_visited[$ - 1] == goal[1]);
  assert(row_visited[0] == player[0]);
  assert(col_visited[0] == player[1]);
  writef("%s\n", row_visited);
  writef("%s\n", col_visited);
  map_destroy(&map);
}
void main() {
}
