import std.stdio;
import std.typecons;
import std.getopt;
import core.thread;
import Dgame.Graphic;
import Dgame.Window.Window;
import Dgame.Window.Event;
import map.map;
import map.map_dgame;
import steering.point;
import steering.game_object;

void main(string[] args) {
  int scene_region_pixels = 10;
  int width = 1280, height = 960;
  int frames = 60, msecs = 1000;
  double distance_accepted = 2;
  int rows = width / scene_region_pixels, cols = height / scene_region_pixels;
  Map *map = map_create(rows, cols);
  Tuple!(int, int)[Size.grid_max * Size.grid_max] actions;
  int[Size.grid_max * Size.grid_max] came_from;
  int[] row_visited;
  int[][] rows_visited;
  int[] col_visited;
  int[][] cols_visited;
  int players;
  int estimate_choice;
  int function(int, int, int, int)[2] estimates = [&manhattan_distance, &euclid_distance];
  int function(int, int, int, int) estimate;
  getopt(args, "players", &players, "estimate_choice", &estimate_choice);
  estimate = estimates[estimate_choice];
  Tuple!(int, int) goal;
  Tuple!(int, int)[] player;
  Shape[] scene_shapes;
  Shape[] player_shape;
  Window window = Window(width, height, "Naviagate");
  Event event;
  Point *seek = point_create();
  Point *position = point_create();
  Point*[] velocity;
  int is_player_shape;
  map_generate(map, 0.05);
  goal = map_place_goal(map);
  writef("%s\n", goal);
  map_q_learning(map, actions, goal[0], goal[1], estimate);
  foreach(i; 0..players) {
    player ~= map_place_player(map);
    writef("%s\n", player[i]);
    map_q_learning_path(came_from, actions, map, goal[0], goal[1], player[i][0], player[i][1]);
    map_extract_path(row_visited, col_visited, came_from, map, goal[0], goal[1], player[i][0], player[i][1]);
    writef("%s\n", row_visited);
    row_visited = row_visited[1..$];
    rows_visited ~= row_visited.dup;
    row_visited.length = 0;
    writef("%s\n", col_visited);
    col_visited = col_visited[1..$];
    cols_visited ~= col_visited.dup;
    col_visited.length = 0;
    map_place_path(map, rows_visited[i], cols_visited[i]);
    velocity ~= point_create();
  }
  map_print(map);
  scene_shapes = map_shapes(map, scene_region_pixels);
  foreach(p; player) {
    player_shape ~= scene_shapes[p[0] * map.cols + p[1]];
  }
  while(true) {
    window.clear();
    while(window.poll(&event)) {
      switch(event.type) {
        case Event.Type.Quit:
          return;
        default:
          break;
      }
    }
    foreach(i, p; player_shape) {
      position.x = p.getPosition().x;
      position.y = p.getPosition().y;
      if(!rows_visited[i].length) {
        continue;
      }
      seek.x = rows_visited[i][0] * scene_region_pixels;
      if(!cols_visited[i].length) {
        continue;
      }
      seek.y = cols_visited[i][0] * scene_region_pixels;
      if(point_distance_f(position.x, position.y, seek.x, seek.y) <= distance_accepted) {
        rows_visited[i] = rows_visited[i][1..$];
        if(!rows_visited[i].length) {
          continue;
        }
        cols_visited[i] = cols_visited[i][1..$];
        if(!cols_visited[i].length) {
          continue;
        }
      }
      game_object_seek_f(velocity[i], position.x, position.y, seek.x, seek.y);
      p.move(velocity[i].x, velocity[i].y);
    }
    foreach(shape; scene_shapes) {
      is_player_shape = 0;
      foreach(p; player_shape) {
        if(shape == p) {
          is_player_shape++;
          break;
        }
      }
      if(is_player_shape) {
        continue;
      }
      window.draw(shape);
    }
    foreach(p; player_shape) {
      window.draw(p);
    }
    window.display();
    Thread.sleep(dur!("msecs")(msecs / frames));
  }
  foreach(v; velocity) {
    point_destroy(&v);
  }
  point_destroy(&position);
  point_destroy(&seek);
  map_destroy(&map);
}
