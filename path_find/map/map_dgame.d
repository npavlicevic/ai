module map.map_dgame;

import Dgame.Math;
import Dgame.Graphic;
import steering.game_object;
import map.map;

Shape[] map_shapes(Map *that, int region_pixels) {
  Shape[] shapes;
  Shape shape;
  Color4b color;
  int row_current;
  int col_current;
  foreach(i; 0..(that.rows * that.cols)) {
    shape = new Shape(Geometry.Quads, [
      Vertex(0, 0),
      Vertex(region_pixels, 0),
      Vertex(region_pixels, region_pixels),
      Vertex(0, region_pixels)
    ]);
    shape.fill = Shape.Fill.Full;
    row_current = cast(int)i / that.cols;
    col_current = cast(int)i - row_current * that.cols;
    shape.setPosition(row_current * region_pixels, col_current * region_pixels);
    switch(that.entries[row_current][col_current]) {
      case Reward.free:
        color = Color4b.White;
        break;
      case Reward.path:
        color = Color4b.Yellow;
        break;
      case Reward.player:
        color = Color4b.Magenta;
        break;
      case Reward.goal:
        color = Color4b.Green;
        break;
      case Reward.obstacle:
        color = Color4b.Black;
        break;
      default:
        break;
    }
    shape.setColor(color);
    shapes ~= shape;
  }
  return shapes;
  // todo change color depending on object
  // todo just use this don't need gameobject
  // todo this in parallel
}
