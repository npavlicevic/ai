module machine.entities;

enum Entities {ent_Miner_Bob, ent_Elsa};
string[] EntitiesStr = ["Miner Bob", "Elsa"];
enum MinerStates {enter_mine_dig, visit_bank, go_home_sleep, quench_thirst};
enum ElsaStates {do_house_work, visit_bathroom};
enum Locations {shack, goldmine, bank, saloon};
enum Messages {msg_hi_honey_im_home, msg_stew_ready};
enum Colors {red, green, yellow, blue, magenta, cyan, reset};
string[] ColorsTxt = ["\x1b[31m", "\x1b[32m", "\x1b[33m", "\x1b[34m", "\x1b[35m", "\x1b[36m", "\x1b[0m"];

void message_receive(T)(T *that, int message) {
  that.messages ~= message;
}
int message_get(T)(T *that) {
  return that.messages[0];
}
void message_pop(T)(T *that) {
  if(that.messages.length > 0) {
    that.messages = that.messages[1..$];
  }
}
