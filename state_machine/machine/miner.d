module machine.miner;

import std.stdio;
import core.stdc.stdlib;
import machine.entities;

struct Miner {
  int id;
  string name;
  int gold_carried;
  int money_in_bank;
  int thirst;
  int fatigue;
  Locations location;
  MinerStates current_state;
  int comfort_level;
  int gold_want;
  int gold_have_thirsty;
  int thirst_quenched;
  int tired;
  int[] messages;
}

Miner *miner_create(int id, string name) {
  Miner *that = cast(Miner*)malloc(Miner.sizeof);
  that.id = id;
  that.name = name;
  that.gold_carried = 0;
  that.money_in_bank = 0;
  that.thirst = 0;
  that.fatigue = 0;
  that.location = Locations.shack;
  that.current_state = MinerStates.go_home_sleep;
  that.comfort_level = 20;
  that.gold_want = 5;
  that.gold_have_thirsty = 3;
  that.thirst_quenched = 5;
  that.tired = 5;
  return that;
}
void miner_destroy(Miner **that) {
  if(*that == null) {
    return;
  }
  free(*that);
  *that = null;
}
bool miner_pockets_full(Miner *that) {
  return that.gold_carried >= that.gold_want;
  // todo make this generic
}
bool miner_thirsty(Miner *that) {
  return that.gold_carried >= that.gold_have_thirsty;
}
bool miner_rich(Miner *that) {
  return that.money_in_bank >= that.comfort_level;
}
bool miner_fatigued(Miner *that) {
  return that.fatigue >= that.tired;
}
void enter_mine_dig_enter(Miner *that) {
  if(that.location != Locations.goldmine) {
    that.location = Locations.goldmine;
    writef("Walkin' to the goldmine\n");
  }
}
int enter_mine_dig_execute(Miner *that) {
  that.gold_carried++;
  that.fatigue++;
  writef("Pickin' up a nugget\n");
  if(miner_pockets_full(that)) {
    return MinerStates.visit_bank;
  }
  if(miner_thirsty(that)) {
    return MinerStates.quench_thirst;
  }
  return MinerStates.enter_mine_dig;
}
void enter_mine_dig_exit(Miner *that) {
  writef("Ah'm leavin' the goldmine with mah pockets full o' sweet gold\n");
}
void visit_bank_deposit_enter(Miner *that) {
  if(that.location != Locations.bank) {
    that.location = Locations.bank;
    writef("Goin' to the bank. Yes siree.\n");
  }
}
int visit_bank_deposit_execute(Miner *that) {
  that.money_in_bank += that.gold_carried;
  that.gold_carried = 0;
  writef("Depositing gold. Total savings now %d.\n", that.money_in_bank);
  if(miner_rich(that)) {
    writef("WooHoo! Rich enough for now. Back home to mah li'lle lady\n");
    return MinerStates.go_home_sleep;
  }
  return MinerStates.enter_mine_dig;
}
void visit_bank_deposit_exit(Miner *that) {
  writef("Leavin' the bank.\n");
}
void go_home_sleep_enter(Miner *that) {
  if(that.location != Locations.shack) {
    that.location = Locations.shack;
    writef("Goin' home.\n");
  }
}
int go_home_sleep_execute(Miner *that) {
  if(!miner_fatigued(that)) {
    return MinerStates.enter_mine_dig;
  }
  that.fatigue = 0;
  writef("ZZZZ....\n");
  return MinerStates.go_home_sleep;
}
void go_home_sleep_exit(Miner *that) {
  writef("Leaving the house.\n");
}
void quench_thirst_enter(Miner *that) {
  if(that.location != Locations.saloon) {
    that.location = Locations.saloon;
    writef("Boy, ah sure is thusty! Walking to the saloon.\n");
  }
}
int quench_thirst_execute(Miner *that) {
  if(miner_thirsty(that)) {
    that.thirst = 0;
    writef("That's mighty fine sippin liquor\n");
    return MinerStates.enter_mine_dig;
  }
  return MinerStates.quench_thirst;
}
void quench_thirst_exit(Miner *that) {
  writef("Leaving the saloon, feelin' good\n");
}
