import std.stdio;
import steering.point;
import steering.game_object;

unittest {
  Point *one = point_create(5, 1);
  Point *two = point_create(5, 3);
  Point *three;
  three = point_diff(one, two);
  assert(three.x == 0);
  assert(three.y == -2);
  point_destroy(&one);
  point_destroy(&two);
  point_destroy(&three);
  one = point_create(5, 3);
  two = point_create(2, 4);
  double angle = point_angle(one, two);
  assert(angle >= 0.56 && angle < 0.57);
  point_destroy(&one);
  point_destroy(&two);
}

void main() {
}
