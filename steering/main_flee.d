import std.stdio;
import core.thread;
import steering.point;
import steering.game_object;

void main() {
  int max = 25, sleep = 800;
  GameObject *craft = game_object_create(1.1, 1.1);
  craft.position[0].x = 3;
  craft.position[0].y = 3;
  Point *dest = point_create();
  dest.x = 4;
  dest.y = 4;
  game_object_flee(craft, dest);
  foreach(i; 0..max) {
    writef("current %f %f, dest %f %f, ", craft.position[0].x, craft.position[0].y, dest.x, dest.y);
    Thread.sleep(dur!("msecs")(sleep));
    game_object_move(craft);
    game_object_acceleration_remove(craft);
    game_object_flee(craft, dest);
    writef("new %f %f\n", craft.position[0].x, craft.position[0].y);
  }
  point_destroy(&dest);
  game_object_destroy(&craft);
}
