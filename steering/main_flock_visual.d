import std.stdio;
import std.random;
import std.parallelism;
import core.thread;
import Dgame.Window.Window;
import Dgame.Window.Event;
import Dgame.Graphic;
import Dgame.Math;
import Dgame.System.Keyboard;
import steering.game_object;

void main() {
  double neighbor_radius = 25;
  int crafts_number = 512, i;
  GameObject*[] crafts;
  Shape _crafts_graphic;
  Shape[] crafts_graphic;
  for(i = 0; i < crafts_number; i++) {
    crafts ~= game_object_create(1.1, 1.1, 3);
  }
  int width = 1280, height = 960, point_offset = 30, x_origin, y_origin;
  foreach(craft; crafts) {
    x_origin = uniform(0, width);
    y_origin = uniform(0, height);
    craft.position[0].x = x_origin;
    craft.position[0].y = y_origin;
    craft.position[1].x = x_origin + point_offset;
    craft.position[1].y = y_origin;
    craft.position[2].x = x_origin + point_offset;
    craft.position[2].y = y_origin + point_offset;
    craft.velocity.x = uniform(0, point_offset) / cast(double)point_offset;
    craft.velocity.y = uniform(0, point_offset) / cast(double)point_offset;
    _crafts_graphic = new Shape(Geometry.Triangles, [
      Vertex(craft.position[0].x, craft.position[0].y),
      Vertex(craft.position[1].x, craft.position[1].y),
      Vertex(craft.position[2].x, craft.position[2].y)
    ]);
    _crafts_graphic.fill = Shape.Fill.Full;
    _crafts_graphic.setColor(Color4b.Black);
    crafts_graphic ~= _crafts_graphic;
  }
  Window window = Window(width, height, "Flock");
  Event event;
  int frames = 60, msecs = 1000;
  while(true) {
    window.clear();
    while(window.poll(&event)) {
      switch(event.type) {
        case Event.Type.Quit:
          return;
        default:
          break;
      }
    }
    foreach(__crafts_graphic; crafts_graphic) {
      window.draw(__crafts_graphic);
    }
    window.display();
    Thread.sleep(dur!("msecs")(msecs / frames));
    foreach(craft; crafts) {
      game_object_cohere(craft, crafts, neighbor_radius);
      game_object_align(craft, crafts, neighbor_radius);
      game_object_separate(craft, crafts, neighbor_radius);
    }
    foreach(j, __crafts_graphic; parallel(crafts_graphic)) {
      if(game_object_off_screen(crafts[j], width, height)) {
        game_object_reverse(crafts[j]);
      }
      game_object_move(crafts[j]);
      __crafts_graphic.move(crafts[j].velocity.x, crafts[j].velocity.y);
      game_object_acceleration_remove(crafts[j]);
    }
  }
  foreach(craft; crafts) {
    game_object_destroy(&craft);
  }
}
