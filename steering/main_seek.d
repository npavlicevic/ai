import std.stdio;
import core.thread;
import steering.point;
import steering.game_object;

void main() {
  int max = 25, sleep = 800;
  double diff_x, diff_y, err = 1;
  GameObject *craft = game_object_create(1.1, 1.1);
  craft.position[0].x = 3;
  craft.position[0].y = 3;
  Point *dest = point_create();
  dest.x = 10;
  dest.y = 7;
  game_object_seek(craft, dest);
  foreach(i; 0..max) {
    writef("current %f %f, dest %f %f, ", craft.position[0].x, craft.position[0].y, dest.x, dest.y);
    Thread.sleep(dur!("msecs")(sleep));
    game_object_move(craft);
    game_object_acceleration_remove(craft);
    game_object_seek(craft, dest);
    writef("new %f %f\n", craft.position[0].x, craft.position[0].y);
    diff_x = craft.position[0].x - dest.x;
    diff_y = craft.position[0].y - dest.y;
    if(diff_x * diff_x + diff_y * diff_y < err) {
      break;
    }
  }
  point_destroy(&dest);
  game_object_destroy(&craft);
}
