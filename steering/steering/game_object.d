module steering.game_object;

import core.stdc.stdlib;
import steering.point;

struct GameObject {
  Point **position;
  int positions;
  Point *acceleration;
  Point *velocity;
  double max_speed;
  double max_force;
}

GameObject *game_object_create(double max_speed = 0, double max_force = 0, int positions = 1) {
  GameObject *that = cast(GameObject*)malloc(GameObject.sizeof);
  that.position = cast(Point**)malloc(positions * Point.sizeof);
  for(int i = 0; i < positions; i++) {
    that.position[i] = point_create();
  }
  that.positions = positions;
  that.acceleration = point_create();
  that.velocity = point_create();
  that.max_speed = max_speed;
  that.max_force = max_force;
  return that;
}
void game_object_destroy(GameObject **that) {
  if(*that == null) {
    return;
  }
  for(int i = 0; i < (*that).positions; i++) {
    point_destroy(&((*that).position[i]));
  }
  free((*that).position);
  point_destroy(&((*that).acceleration));
  point_destroy(&((*that).velocity));
  free(*that);
  *that = null;
}
void game_object_seek(GameObject *that, Point *position) {
  // seek position using seek force
  Point *next_velocity = point_diff(position, that.position[0]);
  point_normalize_set(next_velocity, next_velocity);
  point_mult_scalar_set(next_velocity, next_velocity, that.max_speed);
  // this is next velocity
  Point *seek_force = point_diff(next_velocity, that.velocity);
  // this is seek force, prevents abrupt direction change
  point_truncate_set(seek_force, seek_force, that.max_force);
  that.acceleration.x += seek_force.x;
  that.acceleration.y += seek_force.y;
  point_destroy(&seek_force);
  point_destroy(&next_velocity);
}
void game_object_seek_f(Point *that, double x_one, double y_one, double x_two, double y_two, double max_speed = 1.1, double max_force = 1.1) {
  // seek force using coordinates
  Point *one = point_create(x_one, y_one);
  Point *two = point_create(x_two, y_two);
  Point *diff = point_diff(two, one);
  point_normalize_set(diff, diff);
  point_mult_scalar_set(diff, diff, max_speed);
  Point *diff_velocity = point_diff(diff, that);
  point_normalize_set(diff_velocity, diff_velocity);
  point_mult_scalar_set(diff_velocity, diff_velocity, max_force);
  that.x += diff_velocity.x;
  that.y += diff_velocity.y;
  point_destroy(&one);
  point_destroy(&two);
  point_destroy(&diff);
  point_destroy(&diff_velocity);
}
void game_object_flee(GameObject *that, Point *position) {
  // flee position
  Point *next_velocity = point_diff(that.position[0], position);
  point_normalize_set(next_velocity, next_velocity);
  point_mult_scalar_set(next_velocity, next_velocity, that.max_speed);
  // this is next velocity
  Point *seek_force = point_diff(next_velocity, that.velocity);
  // this is seek force, prevents abrupt direction change
  point_truncate_set(seek_force, seek_force, that.max_force);
  that.acceleration.x += seek_force.x;
  that.acceleration.y += seek_force.y;
  point_destroy(&seek_force);
  point_destroy(&next_velocity);
}
void game_object_cohere(GameObject *that, GameObject*[] neighbors, double neighbor_radius) {
  // look for average position, seek the position
  // can be used for flocking
  double distance, count = 0;
  Point *sum = point_create();
  foreach(neighbor; neighbors) {
    distance = point_distance(that.position[0], neighbor.position[0]);
    if(distance > 0 && distance < neighbor_radius) {
      point_add_set(sum, sum, neighbor.position[0]);
      count++;
    }
  }
  if(count <= 0) {
    return;
  }
  point_div_scalar_set(sum, sum, count);
  game_object_seek(that, sum);
  point_destroy(&sum);
}
void game_object_align(GameObject *that, GameObject*[] neighbors, double neighbor_radius) {
  // align to average velocity of group
  // can be used for flocking
  double distance, count = 0;
  Point *sum = point_create();
  foreach(neighbor; neighbors) {
    distance = point_distance(that.position[0], neighbor.position[0]);
    if(distance > 0 && distance < neighbor_radius) {
      point_add_set(sum, sum, neighbor.velocity);
      count++;
    }
  }
  if(count <= 0) {
    return;
  }
  point_div_scalar_set(sum, sum, count);
  point_truncate_set(sum, sum, that.max_force);
  that.acceleration.x += sum.x;
  that.acceleration.y += sum.y;
  point_destroy(&sum);
  // todo add seek force
}
void game_object_separate(GameObject *that, GameObject*[] neighbors, double neighbor_radius) {
  // seperate elements
  // can be used for flocking
  double distance, count = 0;
  Point *sum = point_create();
  Point *diff = point_create();
  foreach(neighbor; neighbors) {
    distance = point_distance(that.position[0], neighbor.position[0]);
    if(distance > 0 && distance < neighbor_radius) {
      point_diff_set(diff, that.position[0], neighbor.position[0]);
      point_normalize_set(diff, diff);
      point_div_scalar_set(diff, diff, distance);
      point_add_set(sum, sum, diff);
      count++;
    }
  }
  if(count <= 0) {
    return;
  }
  point_div_scalar_set(sum, sum, count);
  point_truncate_set(sum, sum, that.max_force);
  that.acceleration.x += sum.x;
  that.acceleration.y += sum.y;
  point_destroy(&diff);
  point_destroy(&sum);
  // todo add seek force
}
void game_object_move(GameObject *that) {
  // move object
  // add velocity to position
  that.velocity.x += that.acceleration.x;
  that.velocity.y += that.acceleration.y;
  for(int i = 0; i < that.positions; i++) {
    that.position[i].x += that.velocity.x;
    that.position[i].y += that.velocity.y;
  }
}
void game_object_set_acceleration(GameObject *that, double x, double y) {
  that.acceleration.x = x;
  that.acceleration.y = y;
}
void game_object_acceleration_remove(GameObject *that) {
  // remove acceleration
  that.acceleration.x = 0;
  that.acceleration.y = 0;
}
double game_object_rotate(GameObject *that, Point *point) {
  // align with given vector
  double angle = point_angle(that.position[0], point);
  Point *center = point_create();
  for(int position = 0; position < that.positions; position++) {
    point_rotate(that.position[position], center, angle, point_length(that.position[position]));
  }
  point_destroy(&center);
  return angle;
}
bool game_object_off_screen(GameObject *that, double width, double height, double left = 0.5, double right = 1.2) {
  // see if game object is off screen
  return that.position[0].x < -width * left || that.position[0].x > width * right || that.position[0].y < -height * left || that.position[0].y > height * right;
}
void game_object_reverse(GameObject *that, double reverse = 1.1) {
  that.velocity.x += reverse * (-that.velocity.x);
  that.velocity.y += reverse * (-that.velocity.y);
}
